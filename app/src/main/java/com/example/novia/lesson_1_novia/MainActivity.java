package com.example.novia.lesson_1_novia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    float nilai1, nilai2;
    boolean opTambah, opKurang, opKali, opBagi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initNumber0();
        initNumber1();
        initNumber2();
        initNumber3();
        initNumber4();
        initNumber5();
        initNumber6();
        initNumber7();
        initNumber8();
        initNumber9();
        initPeriod();
        initTambah();
        initKurang();
        initBagi();
        initKali();
        initCalculate();
        delete();
        clear();
    }

    public void initCalculate() {
        Button equal = findViewById(R.id.equal);
        equal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textNilai1 = findViewById(R.id.text1);
                TextView textNilai2 = findViewById(R.id.text2);
                TextView textHasil = findViewById(R.id.hasil);
                TextView textOperator = findViewById(R.id.operator);

                String op = textOperator.getText() + "";
                String hasil = textHasil.getText() + "";
                String nilai1String;
                if (hasil != null && !hasil.isEmpty()) {
                    nilai1String = hasil;
                } else {
                    nilai1String = textNilai1.getText() + "";
                }

                String nilai2String = textNilai2.getText() + "";

                if(nilai1String.equals(".")){
                    nilai1String = "0.";
                }

                if(nilai2String.equals(".")){
                    nilai2String = "0.";
                }

                if (op != null && !op.isEmpty()) {
                    if (nilai2String != null && !nilai2String.isEmpty()) {
                        nilai1 = Float.parseFloat(nilai1String);
                        nilai2 = Float.parseFloat(nilai2String);

                        if (opTambah) {
                            nilai1 = nilai1 + nilai2;
                            displayResult(nilai1);
                            opTambah = false;
                        } else if (opKurang) {
                            nilai1 = nilai1 - nilai2;
                            displayResult(nilai1);
                            opKurang = false;
                        } else if (opBagi) {
                            nilai1 = nilai1 / nilai2;
                            displayResult(nilai1);
                            opBagi = false;
                        } else if (opKali) {
                            nilai1 = nilai1 * nilai2;
                            displayResult(nilai1);
                            opKali = false;
                        }

                        textNilai2.setText("");
                        textOperator.setText("");
                    } else {
                        nilai1 = Float.parseFloat(nilai1String);
                        textNilai2.setText("");
                        textOperator.setText("");
                        displayResult(nilai1);
                    }
                }else{
                    if(nilai1String.equals(".")){
                        nilai1 = 0f;
                    }else {
                        nilai1 = Float.parseFloat(nilai1String);
                    }

                    textNilai2.setText("");
                    textOperator.setText("");
                    displayResult(nilai1);
                }
            }
        });
    }

    public void displayResult(Float res) {
        String result;
        if (res % 1 != 0) {
            result = res + "";
        } else {
            Integer resInteger = Math.round(res);
            result = resInteger + "";
        }

        TextView textHasil = findViewById(R.id.hasil);
        textHasil.setText(result);

        TextView textNilai1 = findViewById(R.id.text1);
        textNilai1.setText("");
    }

    public void initTambah() {
        Button tambah = findViewById(R.id.operatorTambah);
        tambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opTambah = true;
                TextView hasil = findViewById(R.id.hasil);
                TextView text1 = findViewById(R.id.text1);
                String nilai1 = text1.getText() + "";
                if(nilai1 != null && !nilai1.isEmpty()) {
                    String hasilString = hasil.getText() + "";
                    if (hasilString != null && !hasilString.isEmpty()) {
                        text1.setText(hasilString);
                    }
                    displayOperator("+");
                }
            }
        });
    }

    public void initKurang() {
        Button kurang = findViewById(R.id.operatorKurang);
        kurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opKurang = true;
                TextView hasil = findViewById(R.id.hasil);
                TextView text1 = findViewById(R.id.text1);
                String hasilString = hasil.getText() + "";
                if (hasilString != null && !hasilString.isEmpty()) {
                    text1.setText(hasilString);
                }
                displayOperator("-");
            }
        });
    }

    public void initBagi() {
        Button bagi = findViewById(R.id.operatorBagi);
        bagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opBagi = true;
                TextView hasil = findViewById(R.id.hasil);
                TextView text1 = findViewById(R.id.text1);
                String hasilString = hasil.getText() + "";
                if (hasilString != null && !hasilString.isEmpty()) {
                    text1.setText(hasilString);
                }
                displayOperator("/");
            }
        });
    }

    public void initKali() {
        Button kali = findViewById(R.id.operatorKali);
        kali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opKali = true;
                TextView hasil = findViewById(R.id.hasil);
                TextView text1 = findViewById(R.id.text1);
                String hasilString = hasil.getText() + "";
                if (hasilString != null && !hasilString.isEmpty()) {
                    text1.setText(hasilString);
                }
                displayOperator("x");
            }
        });
    }

    public void initNumber0() {
        Button number0 = findViewById(R.id.number0);
        number0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("0");
                } else {
                    displayNilai2("0");
                }
            }
        });
    }

    public void initNumber1() {
        Button number1 = findViewById(R.id.number1);
        number1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("1");
                } else {
                    displayNilai2("1");
                }
            }
        });
    }

    public void initNumber2() {
        Button number2 = findViewById(R.id.number2);
        number2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("2");
                } else {
                    displayNilai2("2");
                }
            }
        });
    }

    public void initNumber3() {
        Button number3 = findViewById(R.id.number3);
        number3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("3");
                } else {
                    displayNilai2("3");
                }
            }
        });
    }

    public void initNumber4() {
        Button number4 = findViewById(R.id.number4);
        number4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("4");
                } else {
                    displayNilai2("4");
                }
            }
        });
    }

    public void initNumber5() {
        Button number5 = findViewById(R.id.number5);
        number5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("5");
                } else {
                    displayNilai2("5");
                }
            }
        });
    }

    public void initNumber6() {
        Button number6 = findViewById(R.id.number6);
        number6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("6");
                } else {
                    displayNilai2("6");
                }
            }
        });
    }

    public void initNumber7() {
        Button number7 = findViewById(R.id.number7);
        number7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("7");
                } else {
                    displayNilai2("7");
                }
            }
        });
    }

    public void initNumber8() {
        Button number8 = findViewById(R.id.number8);
        number8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("8");
                } else {
                    displayNilai2("8");
                }
            }
        });
    }

    public void initNumber9() {
        Button number9 = findViewById(R.id.number9);
        number9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView operator = findViewById(R.id.operator);
                String op = operator.getText() + "";
                if (op.equals("")) {
                    displayNilai1("9");
                } else {
                    displayNilai2("9");
                }
            }
        });
    }

    public void clear() {
        Button clear = findViewById(R.id.clear);
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textNilai1 = findViewById(R.id.text1);
                TextView textNilai2 = findViewById(R.id.text2);
                TextView operator = findViewById(R.id.operator);
                TextView hasil = findViewById(R.id.hasil);
                textNilai1.setText("");
                textNilai2.setText("");
                operator.setText("");
                hasil.setText("");
            }
        });
    }

    public void delete() {
        Button del = findViewById(R.id.delete);
        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textNilai1 = findViewById(R.id.text1);
                TextView textNilai2 = findViewById(R.id.text2);
                TextView textOperator = findViewById(R.id.operator);
                TextView hasil = findViewById(R.id.hasil);
                String nilai1 = textNilai1.getText() + "";
                String nilai2 = textNilai2.getText() + "";
                String operator = textOperator.getText() + "";

                if (operator != null && !operator.isEmpty()) {
                    int length = nilai2.length();
                    int last = length - 1;
                    if (length > 0) {
                        nilai2 = nilai2.substring(0, last);
                        textNilai2.setText(nilai2);
                    } else {
                        operator = operator.substring(0, 0);
                        textOperator.setText(operator);
                    }
                } else {
                    int length = nilai1.length();
                    int last = length - 1;
                    if (length > 0) {
                        nilai1 = nilai1.substring(0, last);
                        textNilai1.setText(nilai1);
                    }
                }
                hasil.setText("");
            }
        });
    }

    public void displayNilai1(String number) {
        TextView textNilai1 = findViewById(R.id.text1);
        TextView textHasil = findViewById(R.id.hasil);
        String temp = textNilai1.getText() + "";

        if (temp.equals("0")) {
            textNilai1.setText(number + "");
        } else {
            textHasil.setText("");
            textNilai1.setText(textNilai1.getText() + number + "");
        }
    }

    public void displayNilai2(String number) {
        TextView textNilai2 = findViewById(R.id.text2);
        String temp = textNilai2.getText() + "";

        if (temp.equals("0")) {
            textNilai2.setText(number + "");
        } else {
            textNilai2.setText(textNilai2.getText() + number + "");
        }
    }

    public void displayOperator(String op) {
        TextView operator = findViewById(R.id.operator);
        operator.setText(op + "");
    }

    public void initPeriod() {
        Button period = findViewById(R.id.period);
        period.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textNilai1 = findViewById(R.id.text1);
                TextView textNilai2 = findViewById(R.id.text2);
                TextView textOperator = findViewById(R.id.operator);
                TextView textHasil = findViewById(R.id.hasil);
                String nilai1 = textNilai1.getText() + "";
                String nilai2 = textNilai2.getText() + "";
                String operator = textOperator.getText() + "";

                Pattern sPattern = Pattern.compile("^[^\\.]*$");

                if(nilai1 == null && nilai1.isEmpty()){
                    nilai1 = "0.";
                }else if(nilai2 == null && nilai2.isEmpty()){
                    nilai2 = "0.";
                }

                if (operator != null && !operator.isEmpty()) {
                    Matcher mValue = sPattern.matcher(nilai2);

                    if (mValue.find()) {
                        textNilai2.setText(nilai2 + ".");
                    } else {
                        textNilai2.setText(nilai2 + "");
                    }
                } else {
                    Matcher mValue = sPattern.matcher(nilai1);

                    if (mValue.find()) {
                        textHasil.setText("");
                        textNilai1.setText(nilai1 + ".");
                    } else {
                        textNilai1.setText(nilai1 + "");
                    }
                }
            }
        });
    }
}
